extends Node2D

var platforms = 0

func _process(delta):
	if Input.is_action_just_pressed("left_click"):
		var mouse_position = get_global_mouse_position()
		platforms += 1
		var platform_scene_instance = load("res://Scenes/Platform.tscn").instance()
		platform_scene_instance.set_name(str(platforms))
		platform_scene_instance.global_position = mouse_position
		add_child(platform_scene_instance)
	if Input.is_action_pressed("key_q"):
		if has_node(str(platforms)):
			get_node(str(platforms)).rotation_degrees -= 5
	if Input.is_action_pressed("key_e"):
		if has_node(str(platforms)):
			get_node(str(platforms)).rotation_degrees += 5